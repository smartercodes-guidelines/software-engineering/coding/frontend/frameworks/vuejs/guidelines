# Inspirations

https://vuejs.org/v2/style-guide

You should also take a look at [Some wrong patterns to avoid here](https://www.binarcode.com/blog/3-anti-patterns-to-avoid-in-vuejs/)

See [Best Practices for Writing Vue Apps — Code Organization](https://medium.com/swlh/best-practices-for-writing-vue-apps-code-organization-19e204341c62)
